<?xml version='1.0' encoding="UTF-8" ?>
<!DOCTYPE webpage PUBLIC "-//Norman Walsh//DTD Website V2.5.0//EN"
          "website.dtd"[
  <!ENTITY help SYSTEM "help.xml" NDATA XML>
  <!ENTITY % entities SYSTEM "./entities.xml">
  %entities;
  <!ENTITY % langstats SYSTEM "./langstats.xml">
  %langstats;
  <!ENTITY % qrstats SYSTEM "./qrstats.xml">
  %qrstats;
]>
<webpage id="index">

  <head>
    <title>GIMP Documentation</title>
    <keywords>GIMP, Manual</keywords>
    <meta name="og:title" content="GIMP Documentation" />
    <meta name="og:description" content="Online user manuals and quick reference guides for the GNU Image Manipulation Program" />
    <meta name="og:image" content="http://testing.docs.gimp.org/Layout/202010-wilber-and-co.jpg" />
    <meta name="og:image:alt" content="Illustration by Aryeom Han representing GIMP mascot, Wilber, learning with books" />
    <meta name="og:author" content="GIMP team" />
    <meta name="og:type" content="book" />
    <meta name="book:author" content="GIMP team" />
    <meta name="book:release_date" content="&gimphelp.release.year;" />
  </head>

  <section id="gimp210" xreflabel="2.10">
    <title>GIMP &gimp.release.base; Manuals</title>
<!--
    <para class="Flattr">
      <?dbhtml-include href="includes/flattr.html" ?>
    </para>
-->
    <para>
      This page hosts the online user manuals and quick reference guides for
      GIMP&nbsp;&gimp.release.base;. Manuals for older versions can be found
      at our <ulink url="download.html">Releases</ulink> page.
    </para>
    <para>
      Information about GIMP itself and downloads are available on the
      <ulink url="https://www.gimp.org/">main website</ulink>.
    </para>

    <note>
      <title>Work in Progress</title>
      <para>
        The US English manual, that is used as the base for translations, is
        known to contain errors and omissions. Please note that both this
        manual and the translations are works in progress and can be
        incomplete. We apologize for any inconvenience this may cause.
      </para>
      <para>
        To improve the manual and translations we are looking for more
        volunteers to help us out. If you are interested in writing for the
        English language manual, or in translating the manual, then please
        get in contact with us.
      </para>
    </note>

    <para>
      The online user manuals are updated once a day. After each language
      (except the US English manual), it lists the completion status, i.e. what
      percentage of the manual has been translated.
    </para>

    <variablelist>
      <varlistentry>
        <term id="gimp210-online">Online User Manuals (HTML)</term>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/ca">Català (Catalan)</ulink>
            - &gimphelp.langcode.ca;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/cs">Čeština (Czech)</ulink>
            - &gimphelp.langcode.cs;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/da">Dansk (Danish)</ulink>
            - &gimphelp.langcode.da;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/de">Deutsch (German)</ulink>
            - &gimphelp.langcode.de;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/el">Ελληνικά (Greek)</ulink>
            - &gimphelp.langcode.el;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/en">English (US)</ulink>
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/en_GB">English (British)</ulink>
            - &gimphelp.langcode.en_GB;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/es">Español (Spanish)</ulink>
            - &gimphelp.langcode.es;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/fi">Suomi (Finnish)</ulink>
            - &gimphelp.langcode.fi;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/fr">Français (French)</ulink>
            - &gimphelp.langcode.fr;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/hr">Hrvatski (Croatian)</ulink>
            - &gimphelp.langcode.hr;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/hu">Magyar (Hungarian)</ulink>
            - &gimphelp.langcode.hu;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/it">Italiano (Italian)</ulink>
            - &gimphelp.langcode.it;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/ja">日本語 (Japanese)</ulink>
            - &gimphelp.langcode.ja;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/ko">한국어 (Korean)</ulink>
            - &gimphelp.langcode.ko;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/lt">Lietuvis (Lithuanian)</ulink>
            - &gimphelp.langcode.lt;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/nl">Nederlands (Dutch)</ulink>
            - &gimphelp.langcode.nl;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/nn">Norsk Nynorsk (Norwegian)</ulink>
            - &gimphelp.langcode.nn;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/pt">Português (Portuguese)</ulink>
            - &gimphelp.langcode.pt;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/pt_BR">Português brasileiro (Brazilian Portuguese)</ulink>
            - &gimphelp.langcode.pt_BR;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/ro">Română (Romanian)</ulink>
            - &gimphelp.langcode.ro;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/ru">Pусский (Russian)</ulink>
            - &gimphelp.langcode.ru;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/sl">Slovenščina (Slovenian)</ulink>
            - &gimphelp.langcode.sl;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/sv">Svenska (Swedish)</ulink>
            - &gimphelp.langcode.sv;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/uk">український (Ukrainian)</ulink>
            - &gimphelp.langcode.uk;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/zh_CN">中文 (Simplified Chinese)</ulink>
            - &gimphelp.langcode.zh_CN;%
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Quickreference (PDF)</term>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/pdf/gimp-keys-ca.pdf"
                   title="Catalan">Català (Catalan)</ulink>
            - &gimphelp.langcodeqr.ca;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/pdf/gimp-keys-da.pdf"
                   title="Dansk">Dansk (Danish)</ulink>
            - &gimphelp.langcodeqr.da;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/pdf/gimp-keys-de.pdf"
                   title="Deutsch">Deutsch (German)</ulink>
            - &gimphelp.langcodeqr.de;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/pdf/gimp-keys-el.pdf"
                   title="Greek">Ελληνικά (Greek)</ulink>
            - &gimphelp.langcodeqr.el;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/pdf/gimp-keys-en.pdf"
                   title='English (US)'>English (US)</ulink>
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/pdf/gimp-keys-es.pdf"
                   title="Español">Español (Spanish)</ulink>
            - &gimphelp.langcodeqr.es;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/pdf/gimp-keys-fi.pdf"
                   title='Finnish'>Suomi (Finnish)</ulink>
            - &gimphelp.langcodeqr.fi;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/pdf/gimp-keys-fr.pdf"
                   title="Français">Français (French)</ulink>
            - &gimphelp.langcodeqr.fr;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/pdf/gimp-keys-hut.pdf"
                   title="Magyar">Magyar (Hungarian)</ulink>
            - &gimphelp.langcodeqr.hu;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/pdf/gimp-keys-it.pdf"
                   title="Italiano">Italiano (Italian)</ulink>
            - &gimphelp.langcodeqr.it;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/pdf/gimp-keys-ja.pdf"
                   title="Japanese">日本語 (Japanese)</ulink>
            - &gimphelp.langcodeqr.ja;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/pdf/gimp-keys-ko.pdf"
                   title="Korean">한국어 (Korean)</ulink>
            - &gimphelp.langcodeqr.ko;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/pdf/gimp-keys-nl.pdf"
                   title="Nederlands">Nederlands (Dutch)</ulink>
            - &gimphelp.langcodeqr.nl;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/pdf/gimp-keys-nn.pdf"
                   title="Norwegian">Norsk Nynorsk (Norwegian)</ulink>
            - &gimphelp.langcodeqr.nn;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/pdf/gimp-keys-pl.pdf"
                   title="Polish">Polski (Polish)</ulink>
            - &gimphelp.langcodeqr.pl;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/pdf/gimp-keys-pt.pdf"
                   title="Português">Português (Portuguese)</ulink>
            - &gimphelp.langcodeqr.pt;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/pdf/gimp-keys-pt_BR.pdf"
                   title="Português brasileiro">Português brasileiro (Brazilian Portuguese)</ulink>
            - &gimphelp.langcodeqr.pt_BR;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/pdf/gimp-keys-ro.pdf"
                   title="Română">Română (Romanian)</ulink>
            - &gimphelp.langcodeqr.ro;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/pdf/gimp-keys-ru.pdf"
                   title="Pусский">Pусский (Russian)</ulink>
            - &gimphelp.langcodeqr.ru;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/pdf/gimp-keys-sl.pdf"
                   title="Slovenščina">Slovenščina (Slovenian)</ulink>
            - &gimphelp.langcodeqr.sl;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/pdf/gimp-keys-sv.pdf"
                   title="Svenska">Svenska (Swedish)</ulink>
            - &gimphelp.langcodeqr.sv;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/pdf/gimp-keys-uk.pdf"
                   title="Ukrainian">український (Ukrainian)</ulink>
            - &gimphelp.langcodeqr.uk;%
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="&gimp.release.base;/pdf/gimp-keys-zh_CN.pdf"
                   title="Simplified Chinese">中文 (Simplified Chinese)</ulink>
              - &gimphelp.langcodeqr.zh_CN;%
          </para>
        </listitem>
      </varlistentry>

      <varlistentry id="gimp210-offline">
        <term>Official Releases for Microsoft Windows</term>
        <listitem>
          <para>
            <ulink
              url="https://download.gimp.org/mirror/pub/gimp/help/windows/2.10/">GIMP
              2.10 manual installers for Microsoft Windows (2019-06-28)
            </ulink>
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Manual Source Release for GIMP 2.10</term>
        <listitem>
          <para>
            <ulink
              url="https://download.gimp.org/mirror/pub/gimp/help/gimp-help-2.10.0.tar.bz2"
              title="gimp-help-2.10.0">
              2.10.0 (2019-06-21, 179 MB)
            </ulink>
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>Source Code</term>
        <listitem>
          <para>
            The repository for the GIMP Help Manual can be found at
            <ulink url="https://gitlab.gnome.org/GNOME/gimp-help"
              title="GIMP Help Manual repository">https://gitlab.gnome.org/GNOME/gimp-help</ulink>.
          </para>
        </listitem>
        <listitem>
          <para>
            Use <ulink url="https://git-scm.com/" title="Download
              Git">git</ulink> to grab the source code from:

            <programlisting>
              <![CDATA[
              $ git clone git@gitlab.gnome.org:GNOME/gimp-help.git
              ]]>
            </programlisting>
          </para>
        </listitem>
        <listitem>
          <para>
            The <olink targetdocent="help">Help us</olink> page provides
            more information if you would like to help us.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </section>
</webpage>
