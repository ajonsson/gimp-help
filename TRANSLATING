How To Add a New Translation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. The easiest is probably to open an issue requesting for your language to
   be added: https://gitlab.gnome.org/GNOME/gimp-help/-/issues
   A more complete write-up of how to add a new language for maintainers of
   gimp-help is available in the README.md

2. If you don't want to wait for that:

3. Clone the git@gitlab.gnome.org:GNOME/gimp-help.git repository.

4. Open 'configure.ac' file in a text editor.

5. Go to this line:

ALL_LINGUAS="ca cs da de el en en_GB es fi fr hr hu it ja ko lt nl nn pt pt_BR ro ru sl sv uk zh_CN"

6. Add a two-letter or four-letter code for your language, e.g. 'ro' for
   Romanian or 'zh_CN' for Chinese (Simplified). Save the file.

7. Run './autogen.sh' to generate a Makefile.

8. Run 'make po-LANG' to generate PO files for your language, where LANG
   is either the two-letter or four-letter code that you added.


How To Translate
~~~~~~~~~~~~~~~~

Translations should follow the GNOME Translation Project's workflow,
using the Damned Lies translation platform. For more information, see
https://wiki.gnome.org/TranslationProject.

Use any PO editor you prefer (Poedit, Gtranslator, Lokalize etc.) to open
and translate PO files create at the previous step. Don't forget to fill
in basic metadata like translator's name and email address.


How To Test Translation
~~~~~~~~~~~~~~~~~~~~~~~

Run 'make html-LANG', where LANG is your two/four-letter language code,
to generate HTML version of the user manual.


How To Add Localized Screenshots
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

All images are stored in the top-level 'images' folder. Original
screenshots in English are in the 'C' subfolder. Localized screenshots
are in 'LANG' folders, where LANG is your two/four-letter language code.

It's important to preserve both paths and names when saving localized
versions of the screenshots. Otherwise documentation won't build properly,
if at all.


How To Submit Translation
~~~~~~~~~~~~~~~~~~~~~~~~~

There are multiple ways to submit your translation depending on the access
level you have for the Git repository. 

If you cloned the Git repository anonymously, and you intend to either send
the files to your GNOME i18n team or submit a patch to the bug tracker, do
this:

1. Add all the files you created ('git add') and commit them ('git commit').

2. Run 'git pull --rebase' to sync your local copy to the main remote Git
repository.

3. Run 'git format-patch origin/master' to generate Git-formatted patch 
(or patches).

4. Submit newly created files.

If you cloned the Git repository via SSH, it means you already have the
access to push directly to the remote repository. Simply run add and commit
all your changes, then run 'git push origin master'.
