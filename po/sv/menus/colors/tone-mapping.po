# Swedish translation for GIMP.
# Copyright © 2022 Free Software Foundation, Inc.
# This file is distributed under the same license as the gimp-help package.
# Anders Jonsson <anders.jonsson@norsjovallen.se>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: gimp-help tone-mapping\n"
"POT-Creation-Date: 2021-06-17 18:26+0000\n"
"PO-Revision-Date: 2022-02-20 13:47+0100\n"
"Last-Translator: Anders Jonsson <anders.jonsson@norsjovallen.se>\n"
"Language-Team: Swedish <tp-sv@listor.tp-sv.se>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.0.1\n"

#: src/menus/colors/tone-mapping/reinhard.xml:9(title)
msgid "Reinhard 2005"
msgstr "Reinhard 2005"

#: src/menus/colors/tone-mapping/reinhard.xml:10(para)
#: src/menus/colors/tone-mapping/mantiuk.xml:10(para)
#: src/menus/colors/tone-mapping/stress.xml:10(para)
#: src/menus/colors/tone-mapping/fattal.xml:10(para)
msgid "TODO"
msgstr "Kvarstår att skriva"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: src/menus/colors/tone-mapping/retinex.xml:32(None)
msgid ""
"@@image: 'images/filters/examples/colors-retinex1.png'; "
"md5=25f4bc072d9614e58d5cb74fd9a7f70e"
msgstr ""
"@@image: 'images/filters/examples/colors-retinex1.png'; "
"md5=25f4bc072d9614e58d5cb74fd9a7f70e"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: src/menus/colors/tone-mapping/retinex.xml:41(None)
msgid ""
"@@image: 'images/filters/examples/colors-retinex2.png'; "
"md5=239358a7b963e67f1eac7fbebb89fb73"
msgstr ""
"@@image: 'images/filters/examples/colors-retinex2.png'; "
"md5=239358a7b963e67f1eac7fbebb89fb73"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: src/menus/colors/tone-mapping/retinex.xml:85(None)
msgid ""
"@@image: 'images/menus/colors/retinex.png'; "
"md5=94377a51b24152a3cd9a0f290de9ca40"
msgstr ""
"@@image: 'images/menus/colors/retinex.png'; "
"md5=94377a51b24152a3cd9a0f290de9ca40"

#: src/menus/colors/tone-mapping/retinex.xml:14(title)
#: src/menus/colors/tone-mapping/retinex.xml:19(tertiary)
#: src/menus/colors/tone-mapping/retinex.xml:22(primary)
msgid "Retinex"
msgstr "Retinex"

#: src/menus/colors/tone-mapping/retinex.xml:17(primary)
msgid "Colors"
msgstr "Färger"

#: src/menus/colors/tone-mapping/retinex.xml:18(secondary)
msgid "Tone Mapping"
msgstr "Tonmappning"

#: src/menus/colors/tone-mapping/retinex.xml:26(title)
msgid "Overview"
msgstr "Översikt"

#: src/menus/colors/tone-mapping/retinex.xml:28(title)
msgid "<quote>Retinex</quote> example"
msgstr "Exempel för <quote>Retinex</quote>"

#: src/menus/colors/tone-mapping/retinex.xml:35(para)
msgid "Original image"
msgstr "Ursprunglig bild"

#: src/menus/colors/tone-mapping/retinex.xml:44(para)
msgid ""
"<quote>Retinex</quote> filter applied. Note new details in the upper right "
"corner."
msgstr ""
"Filtret <quote>Retinex</quote> tillämpat. Observera nya detaljer i övre "
"högra hörnet."

#: src/menus/colors/tone-mapping/retinex.xml:51(para)
msgid ""
"Retinex improves visual rendering of an image when lighting conditions are "
"not good. While our eye can see colors correctly when light is low, cameras "
"and video cams can't manage this well. The MSRCR (MultiScale Retinex with "
"Color Restoration) algorithm, which is at the root of the Retinex filter, is "
"inspired by the eye biological mechanisms to adapt itself to these "
"conditions. Retinex stands for Retina + cortex."
msgstr ""
"Retinex förbättrar visuell rendering av en bild när ljusförhållanden inte är "
"bra. Medan vårt öga kan se färger korrekt när belysningen är dålig så kan "
"kameror och videokameror inte hantera detta bra. MSRCR-algoritmen "
"(MultiScale Retinex with Color Restoration, flerskale-Retinex med "
"färgåterställning), som ligger i botten för Retinex-filtret, har inspirerats "
"av ögats biologiska mekanismer för att anpassa sig till dessa villkor. "
"Retinex står för Retina + cortex."

# TODO: scanners -> scans?
#: src/menus/colors/tone-mapping/retinex.xml:59(para)
msgid ""
"Besides digital photography, Retinex algorithm is used to make the "
"information in astronomical photos visible and detect, in medicine, poorly "
"visible structures in X-rays or scanners."
msgstr ""
"Förutom digitalfotografi används Retinex-algoritmen för att göra "
"informationen i astronomiska foton synlig samt för att i medicinen upptäcka "
"svagt synliga strukturer i röntgenfoton eller avbilder."

#: src/menus/colors/tone-mapping/retinex.xml:67(title)
msgid "Activate the filter"
msgstr "Aktivera filtret"

#: src/menus/colors/tone-mapping/retinex.xml:68(para)
msgid ""
"This filter is found in the image window menu under "
"<menuchoice><guimenu>Colors</guimenu><guisubmenu>Tone Mapping</"
"guisubmenu><guimenuitem>Retinex…</guimenuitem></menuchoice>."
msgstr ""
"Detta filter hittas i bildfönstermenyn under <menuchoice><guimenu>Färger</"
"guimenu><guisubmenu>Tonmappning</guisubmenu><guimenuitem>Retinex…</"
"guimenuitem></menuchoice>."

#: src/menus/colors/tone-mapping/retinex.xml:79(title)
msgid "Options"
msgstr "Alternativ"

#: src/menus/colors/tone-mapping/retinex.xml:81(title)
msgid "<quote>Retinex</quote> filter options"
msgstr "Alternativ för filtret <quote>Retinex</quote>"

# TODO: experimented -> experienced
#: src/menus/colors/tone-mapping/retinex.xml:89(para)
msgid ""
"These options call for notions that only mathematicians and imagery "
"engineers can understand. In actual practice, the user has to grope about "
"for the best setting. However, the following explanations should help out "
"the experimented GIMP user."
msgstr ""
"Dessa alternativ efterfrågar begrepp som bara matematiker och bildingenjörer "
"kan förstå. I praktiken kommer användaren behöva famla runt efter den bästa "
"inställningen. Följande förklaringar bör dock hjälpa den erfarna GIMP-"
"användaren."

#: src/menus/colors/tone-mapping/retinex.xml:97(term)
msgid "Level"
msgstr "Nivå"

# TODO: lightor -> light source? French original: Pour caractériser les variations couleurs et l'illuminant on effectue une différence d'intensité entre les réponses des filtres (gaussien) à différentes échelles.
#: src/menus/colors/tone-mapping/retinex.xml:99(para)
msgid ""
"Here is what the plug-in author writes on his site <xref "
"linkend=\"bibliography-online-plugin-retinex\"/>: <quote>To characterize "
"color variations and the lightor, we make a difference between (gaussian) "
"filters responses at different scales. These parameters allow to specify how "
"to allocate scale values between min scale (sigma 2.0) and max (sigma equal "
"to the image size)</quote>..."
msgstr ""
"Här är vad insticksmodulens författare skriver på sin sida <xref "
"linkend=\"bibliography-online-plugin-retinex\"/>: <quote>För att "
"karakterisera färgvariationer och belysning, gör vi skillnad mellan "
"(gaussiska) filtersvaren vid olika skalor. Dessa parametrar låter oss ange "
"hur skalvärden ska allokeras mellan minimal skala (sigma 2,0) och maximal "
"(sigma lika med bildstorleken)</quote>…"

#: src/menus/colors/tone-mapping/retinex.xml:110(term)
msgid "Uniform"
msgstr "Enhetlig"

#: src/menus/colors/tone-mapping/retinex.xml:112(para)
msgid "Uniform tends to treat both low and high intensity areas fairly."
msgstr ""
"Enhetlig tenderar att behandla både områden med hög och låg intensitet "
"rättvist."

#: src/menus/colors/tone-mapping/retinex.xml:119(term)
msgid "Low"
msgstr "Låg"

#: src/menus/colors/tone-mapping/retinex.xml:121(para)
msgid ""
"As a rule of thumb, low does <quote>flare up</quote> the lower intensity "
"areas on the image."
msgstr ""
"Låg låter som en tumregel områdena med lägre intensitet i bilden "
"<quote>flamma upp</quote>."

#: src/menus/colors/tone-mapping/retinex.xml:128(term)
msgid "High"
msgstr "Hög"

#: src/menus/colors/tone-mapping/retinex.xml:130(para)
msgid ""
"High tends to <quote>bury</quote> the lower intensity areas in favor of a "
"better rendering of the clearer areas of the image."
msgstr ""
"Hög tenderar att <quote>begrava</quote> områdena med lägre intensitet till "
"förmån för bättre rendering av de klarare områdena i bilden."

#: src/menus/colors/tone-mapping/retinex.xml:141(term)
msgid "Scale"
msgstr "Skala"

#: src/menus/colors/tone-mapping/retinex.xml:143(para)
msgid ""
"Determines the depth of the Retinex scale. Minimum value is 16, a value "
"providing gross, unrefined filtering. Maximum value is 250. Optimal and "
"default value is 240."
msgstr ""
"Avgör djupet på Retinex-skalan. Minsta värdet är 16, ett värde som "
"tillhandahåller hemsk, oraffinerad filtrering. Högsta värdet är 250. "
"Optimalt värde är 240 som även är standard."

#: src/menus/colors/tone-mapping/retinex.xml:151(term)
msgid "Scale division"
msgstr "Skaldelning"

#: src/menus/colors/tone-mapping/retinex.xml:153(para)
msgid ""
"Determines the number of iterations in the multiscale Retinex filter. The "
"minimum required, and the recommended value is three. Only one or two scale "
"divisions removes the multiscale aspect and falls back to a single scale "
"Retinex filtering. A value that is too high tends to introduce noise in the "
"picture."
msgstr ""
"Avgör antalet iterationer i Retinex-flerskalefiltret. Det minsta som krävs, "
"samt även rekommenderat värde är 3. Endast en eller två skaldelningar tar "
"bort flerskaleaspekten och faller tillbaka på en Retinex-filtrering i "
"enkelskala. Ett värde som är för högt tenderar att introducera brus i bilden."

#: src/menus/colors/tone-mapping/retinex.xml:163(term)
msgid "Dynamic"
msgstr "Dynamik"

#: src/menus/colors/tone-mapping/retinex.xml:165(para)
msgid ""
"As the MSR algorithm tends to make the image lighter, this slider allows you "
"to adjust color saturation contamination around the new average color. A "
"higher value means less color saturation. This is definitely the parameter "
"you want to tweak for optimal results, because its effect is extremely image-"
"dependent."
msgstr ""
"Då MSR-algoritmen tenderar att göra bilden ljusare så låter detta "
"skjutreglage dig justera färgmättnadskontamineringen runt den nya "
"medelvärdesfärgen. Ett högre värde betyder mindre färgmättnad. Detta är "
"absolut parametern som du vill justera för optimala resultat, då dess effekt "
"är extremt bildberoende."

#: src/menus/colors/tone-mapping/mantiuk.xml:9(title)
msgid "Mantiuk 2006"
msgstr "Mantiuk 2006"

#: src/menus/colors/tone-mapping/stress.xml:9(title)
msgid "Stress"
msgstr "Belasta"

#: src/menus/colors/tone-mapping/fattal.xml:9(title)
msgid "Fattal et al. 2002"
msgstr "Fattal et al. 2002"

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: src/menus/colors/tone-mapping/fattal.xml:0(None)
msgid "translator-credits"
msgstr "Anders Jonsson <anders.jonsson@norsjovallen.se>, 2022"
