image: debian:testing

stages:
  - prepare
  - build
  - www
  - distribution

variables:
  INSTALL_DIR: "_install"
  INSTALL_PREFIX: "${CI_PROJECT_DIR}/${INSTALL_DIR}"

build-image:
  stage: prepare
  variables:
    GIT_STRATEGY: none
  cache: {}
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - echo "FROM debian:testing" > Dockerfile
    - echo "RUN apt-get update" >> Dockerfile
    - echo "RUN apt-get install -y --no-install-recommends \\" >> Dockerfile
    - echo "automake build-essential docbook-website docbook-xml docbook-xsl fonts-noto-cjk gettext intltool librsvg2-2 librsvg2-bin pkg-config pngcrush python3 python3-libxml2 xsltproc" >> Dockerfile

    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE:build-debian-latest --cache=true --cache-ttl=120h

.build-debian-base:
  stage: build
  image: $CI_REGISTRY_IMAGE:build-debian-latest
  artifacts:
    expire_in: 1 week
    when: always
    paths:
    - "${HELP_DIR}"
    - "${PDF_DIR}"
  script:
    - mkdir _build
    - cd _build
    - ../autogen.sh --prefix=${INSTALL_PREFIX} --without-gimp
    - make && make install
    - mv ${INSTALL_PREFIX}/share/gimp/3.0/help/ ../${HELP_DIR}
    - mv ../${HELP_DIR}/pdf/ ../${PDF_DIR}
  timeout: 2 hours 30 minutes
  needs: ["build-image"]

build-debian-1:
  extends: .build-debian-base
  variables:
    LINGUAS: "ca cs da de el en en_GB"
    HELP_DIR: "_html1"
    PDF_DIR: "_pdf1"

build-debian-2:
  extends: .build-debian-base
  variables:
    LINGUAS: "es fi fr hr hu it ja"
    HELP_DIR: "_html2"
    PDF_DIR: "_pdf2"

build-debian-3:
  extends: .build-debian-base
  variables:
    LINGUAS: "ko lt nl nn pt pt_BR"
    HELP_DIR: "_html3"
    PDF_DIR: "_pdf3"

build-debian-4:
  extends: .build-debian-base
  variables:
    LINGUAS: "ro ru sl sv uk zh_CN"
    HELP_DIR: "_html4"
    PDF_DIR: "_pdf4"

www-debian:
  stage: www
  image: $CI_REGISTRY_IMAGE:build-debian-latest
  dependencies:
    - build-debian-1
    - build-debian-2
    - build-debian-3
    - build-debian-4
  artifacts:
    expire_in: 1 week
    when: always
    paths:
    - build/windows/installer/lang/
    - htdocs/
  script:
    # build the installer language files only.
    - mkdir _build
    - cd _build
    - ../autogen.sh --prefix=${INSTALL_PREFIX} --without-gimp --enable-windows-installer
    - cd build/windows/installer/lang
    - make
    - cd ../../../..
    - mv build/windows/installer/lang/help*isl ../build/windows/installer/lang/
    # build the landing pages.
    - cd ../web
    - make
    - cd ..
    - mv /html/ htdocs/
    # Move all PDF quickreferences
    - MAJOR_VERSION=`grep 'm4_define(\[help_major_version' configure.ac |sed 's/m4_define(\[help_major_version.*\[\([0-9]*\)\].*/\1/'`
    - MINOR_VERSION=`grep 'm4_define(\[help_minor_version' configure.ac |sed 's/m4_define(\[help_minor_version.*\[\([0-9]*\)\].*/\1/'`
    - mkdir -p htdocs/${MAJOR_VERSION}.${MINOR_VERSION}/pdf
    - mv _pdf1/*.pdf htdocs/${MAJOR_VERSION}.${MINOR_VERSION}/pdf/
    - mv _pdf2/*.pdf htdocs/${MAJOR_VERSION}.${MINOR_VERSION}/pdf/
    - mv _pdf3/*.pdf htdocs/${MAJOR_VERSION}.${MINOR_VERSION}/pdf/
    - mv _pdf4/*.pdf htdocs/${MAJOR_VERSION}.${MINOR_VERSION}/pdf/
    # Move all the help html files.
    - mv _html1/* htdocs/${MAJOR_VERSION}.${MINOR_VERSION}/
    - mv _html2/* htdocs/${MAJOR_VERSION}.${MINOR_VERSION}/
    - mv _html3/* htdocs/${MAJOR_VERSION}.${MINOR_VERSION}/
    - mv _html4/* htdocs/${MAJOR_VERSION}.${MINOR_VERSION}/

win-installer:
  variables:
    CHERE_INVOKING: "yes"
  tags:
    - win32-ps
  stage: distribution
  dependencies:
    - www-debian
  artifacts:
    name: "${CI_JOB_NAME}-${CI_COMMIT_REF_SLUG}"
    when: always
    expire_in: 1 week
    paths:
    - build/windows/installer/_Output
    - installer.log
  script:
    - C:\msys64\usr\bin\pacman --noconfirm -Syyuu
    - C:\msys64\usr\bin\bash -lc "bash -x ./build/windows/installer/installer-gimp-help-msys2.sh > installer.log 2>&1"
