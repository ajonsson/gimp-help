<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect2 PUBLIC "-//OASIS//DTD DocBook XML V4.3//EN"
                       "http://www.docbook.org/xml/4.3/docbookx.dtd">

<!--Section History
  2019-12-18 j.h: add on-canvas layer selection
-->

<sect1 id="gimp-layer-groups">
  <title>Layer Groups</title>
	
	<indexterm>
    <primary>Layer groups</primary>
  </indexterm>
	<indexterm>
    <primary>Pass Through</primary>
  </indexterm>
  <indexterm>
    <primary>Layer</primary>
    <secondary>On-canvas layer selection</secondary>
  </indexterm>

  <para>
    Layer Groups are available in <acronym>GIMP</acronym> 2.8 and newer.
  </para>

  <para>
    You can group layers that have similarities in a tree-like way. So, the
    layer list becomes easier to manage.
  </para>

  <mediaobject>
    <imageobject>
      <imagedata format="PNG"
        fileref="images/dialogs/layer-group.png"/>
    </imageobject>
  </mediaobject>

  <variablelist>
    <varlistentry>
      <term>Create a Layer Group</term>
      <listitem>
        <para>
          You can create a layer group by clicking the
          <guibutton>New Layer Group</guibutton>
          button at the bottom of the layers dialog,
        </para>
        <para>
          through
          <menuchoice>
            <guimenu>Layer</guimenu>
            <guimenuitem>New Layer Group</guimenuitem>
          </menuchoice>,
          or through the Layers dialog context menu.
        </para>
        <para>
          This empty layer group appears just above the current layer. It is
          recommended to give it a name that is descriptive.  To change
          the layer group name, double-click the name, or press 
          <keycap>F2</keycap> the name, or right-click it and then select
          the <command>Edit Layer Attributes</command> in the context menu.
          If you don't rename your layer groups, you will get confused when several 
          are created with names such as Layer Group #1, Layer Group #2, etc.
        </para>
        <para>
          You can create several layer groups and you can
          <emphasis role="bold">embed</emphasis> them, that is include a
          layer group in another one.
        </para>
      </listitem>
    </varlistentry>

    <varlistentry>
      <term>Adding Layers to a Layer Group</term>
      <listitem>
        <para>
          You can add <emphasis>existing layers</emphasis> to a layer group
          by click-and-dragging them.
          <note>
            <para>
              The hand representing the mouse pointer must turn smaller
              before releasing the mouse button.
            </para>
            <para>
              A thin horizontal line marks where the layer will be laid
              down.
            </para>
          </note>
        </para>
        <para>
          To add a <emphasis>new layer</emphasis> to the current layer
          group,
          click the <guibutton>New Layer...</guibutton> at the
          bottom of the Layers dialog, or use the
          <guimenuitem>New Layer</guimenuitem> command in the image menu
          or press <keycombo><keycap>Shift</keycap><keycap>Ctrl</keycap><keycap>N</keycap></keycombo>.
        </para>
        <para>
          When a layer group is not empty, a small <quote>></quote> icon
          appears. By clicking it, you can fold or unfold the layer list.
          <mediaobject>
            <imageobject>
              <imagedata format="PNG"
                fileref="images/dialogs/fold-unfold.png"/>
            </imageobject>
          </mediaobject>
        </para>
        <para>
          Layers that belong to a layer group are slightly indented to the
          right, allowing you know easily which layers are part of the
          group.
        </para>
      </listitem>
    </varlistentry>
    
    <varlistentry>
      <term>Visibility</term>
      <listitem>
        <para>
          If a layer group is made invisible using the eye icon but still open 
          (so that the layers inside the group are shown in the list), there is 
          a struck out eye shown besides the layers that are inside the 
          group to indicate that these layers are not displayed in the final 
          projection of the image, but theoretically visible in the layer group.
        </para>
        <mediaobject>
          <imageobject>
            <imagedata format="PNG"
              fileref="images/dialogs/layer-group-visibility.png"/>
          </imageobject>
        </mediaobject>
      </listitem>
    </varlistentry>
    
    <varlistentry>
      <term>Raise and Lower Layer Groups</term>
      <listitem>
        <para>
          You can raise and lower layer groups in the Layer dialog as you do
          with normal layers: click-and-dragging, and by using 
          <keycap>up arrow</keycap> and <keycap>down arrow</keycap>
          keys at the bottom of the Layer dialog.
        </para>
      </listitem>
    </varlistentry>

    <varlistentry>
      <term>Duplicate a Layer Group</term>
      <listitem>
        <para>
          To duplicate a layer group, click the
          <guibutton>Create a duplicate of the layer</guibutton> button or
          right-click and select the <command>Duplicate Layer</command>
          command in the pop up context menu.
        </para>
      </listitem>
    </varlistentry>

    <varlistentry>
      <term>Move Layer Groups</term>
      <listitem>
        <para>
          You can <emphasis role="bold">move a layer group to another image
          </emphasis> by click-and-dragging.
          You can also copy-paste it using <keycombo><keycap>Ctrl</keycap><keycap>C</keycap></keycombo>
          and <keycombo><keycap>Ctrl</keycap><keycap>V</keycap></keycombo>: then, you get
          a floating selection that you must anchor (anchor button at the
          bottom of the Layer dialog).
        </para>
        <para>
          You can also <emphasis role="bold">move a layer group to the
          canvas</emphasis>: this duplicates the group <emphasis>in</emphasis>
          the group. Chain all layers in the duplicated layer group, select
          the Move tool, then, in the image, move the layer. That's one way to
          multiply multi-layer objects in an image.
        </para>
      </listitem>
    </varlistentry>
    
    <varlistentry>
      <term>Delete a Layer Group</term>
      <listitem>
        <para>
          To delete a layer group, click the <guibutton>red cross</guibutton>
          button at the
          bottom of the Layer dialog or right-click and select <command>
          Delete layer</command>.
        </para>
      </listitem>
    </varlistentry>

    <varlistentry>
      <term>Embed Layer Groups</term>
      <listitem>
        <para>
          When a layer group is activated, you can add another group inside
          it with the <quote>Add New Layer Group</quote> command. There
          seems to be no limit, excepted memory, to the number of embedded
          layer groups.
        </para>
      </listitem>
    </varlistentry>

    <varlistentry>
      <term>Layer Modes and Groups</term>
      <listitem>
        <para>
          A layer mode applied to a layer group acts on layers that are in this
          group only. A layer mode above a layer group acts on all layers
          underneath, outside and inside the layer groups.
        </para>
        <mediaobject>
          <imageobject>
            <imagedata format="PNG"
              fileref="images/dialogs/layer-group-original.png"/>
          </imageobject>
          <caption>
            <para>
              Original image
            </para>
          </caption>
        </mediaobject>
        <figure>
          <title>Layer Mode in or out Layer Group</title>
          <mediaobject>
            <imageobject>
              <imagedata format="PNG"
                fileref="images/dialogs/layer-group-merge-in.png"/>
            </imageobject>
            <caption>
              <para>
                We added a white layer <emphasis>in</emphasis> the layer group
                with saturation mode: only square and triangle are disabled.
              </para>
            </caption>
          </mediaobject>
          <mediaobject>
            <imageobject>
              <imagedata format="PNG"
                fileref="images/dialogs/layer-group-merge-out.png"/>
            </imageobject>
            <caption>
              <para>
                We added a white layer <emphasis>out</emphasis> of the layer
                group with saturation mode: all layers underneath are disabled, 
                including the background layer.
              </para>
            </caption>
          </mediaobject>
        </figure>
        
        <para>
          With <acronym>GIMP</acronym> 2.10 and newer, layer groups have a special layer mode: the 
          <guilabel>Pass Through</guilabel> mode. This mode exists only if a 
          layer group is active.
        </para>
        <para>
          When this mode is used instead of any other one, layers inside the 
					layer group will behave as if they were a part of the layer stack, 
					not belonging to the group. Layers within the group blend with layers 
					below, inside and outside the group. 
				</para>
				<para>
					While with Normal mode, layers within a group are treated as if they 
					were a single layer, which is then blended with other layers below in 
					the stack; a modifier on a layer inside the group blends layers below 
					in the group only.
				</para>
				<para>
					More details about Pass Through in 
					<xref linkend="glossary-pass-through"/>.
				</para>
      </listitem>
    </varlistentry>

    <varlistentry>
      <term>Opacity</term>
      <listitem>
        <para>
          When a layer group is activated, opacity changes are applied to all
          the layers of the group.
        </para>
      </listitem>
    </varlistentry>

    <varlistentry>
      <term>Layer Mask</term>
      <listitem>
        <para>
					With <acronym>GIMP</acronym> 2.10 and newer, masks on layer groups are possible. They work similarly to 
					ordinary-layer masks, with the following considerations.
				</para>
				<para>
					The group’s mask size is the same as group’s size (i.e., the bounding box of its children) at all times. When the group’s size changes, the mask is cropped to the new size — areas of the mask that fall outside of the new bounds are discarded, and newly added areas are filled with black (and hence are transparent by default).
				</para>
				<para>
					 Of course, you still can add a layer mask to a layer in the group to mask a part of the layer:
        </para>
        <mediaobject>
          <imageobject>
            <imagedata format="PNG"
              fileref="images/dialogs/layer-group-mask.png"/>
          </imageobject>
          <caption>
            <para>
              We added a white (Full opacity) layer mask to the triangle layer.
            </para>
          </caption>
        </mediaobject>
      </listitem>
    </varlistentry>
    
    <varlistentry>
      <term>Finding a layer</term>
      <listitem>
        <para>
          When working with a lot of layers, finding a particular layer in the 
          list may be difficult. With <acronym>GIMP</acronym> 2.10.10 and newer, a new 
          <emphasis>on-canvas layer selection</emphasis> function is available. 
          <keycombo><keycap>Alt</keycap><mousebutton>middle click</mousebutton>
          </keycombo> the image element you want to find the layer this 
          element belongs to: the available layers will be looped through to 
          show the new active layer and the layer name will be temporarily 
          displayed in the status bar.
        </para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term>Layer preview</term>
      <listitem>
        <para>
          There were problems with drawable preview rendering in the case of a 
          large image consisting of many layers. This problem is fixed with
          <acronym>GIMP</acronym> 2.10.10 and newer, except for layer 
          groups. You can disable rendering layer group previews in 
          <menuchoice>
            <guimenu>Edit</guimenu>
            <guisubmenu>Preferences</guisubmenu>
            <guimenuitem>Interface</guimenuitem>
          </menuchoice>.
        </para>
      </listitem>
    </varlistentry>
  </variablelist>

</sect1>


