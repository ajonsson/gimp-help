<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect2 PUBLIC "-//OASIS//DTD DocBook XML V4.3//EN"
                "http://www.docbook.org/xml/4.3/docbookx.dtd">

<!-- section history:
  2088-12-22 j.h: updated to v2.6
  2008-01-04 ude: replaced calloutlist with orderedlist
  2007-02-07 Added 'no' by KoSt
  2006-02-26 j.h:  Added Right click -> Open location-Changed left pannel en;fr
  2005/10/18 zh_CN removed by romanofski - missing translation
-->
<sect2 id="gimp-file-open">
  <title>File Open…</title>

  <indexterm>
    <primary>Image</primary>
    <secondary>Open</secondary>
  </indexterm>
  <indexterm><primary>Open</primary></indexterm>

  <para>
    The <guimenuitem>Open…</guimenuitem> command activates a dialog that lets
    you select an image to be loaded from your hard-drive or an external device.
    For other ways of opening files, see the commands described on the next
    pages (<xref linkend="gimp-file-open-as-layer"/> etc.).
  </para>

  <sect3>
    <title>Activate Dialog</title>

    <itemizedlist>
      <listitem>
        <para>
          You can access the <guimenuitem>Open</guimenuitem> dialog
          from the menu through:
          <menuchoice>
            <guimenu>File</guimenu>
            <guimenuitem>Open…</guimenuitem>
          </menuchoice>.
        </para>
      </listitem>
      <listitem>
        <para>
          You can also open this dialog by using the keyboard shortcut
          <keycombo><keycap>Ctrl</keycap><keycap>O</keycap></keycombo>.
        </para>
      </listitem>
    </itemizedlist>
  </sect3>

  <sect3>
    <title>File browsing</title>

    <figure>
      <!--OLD: file-open-dialog.xcf.gz in src/images/menus-->
      <!--Since 2.6: file-open.xcf.gz in src/images/menus-->
      <title>The Open Image Dialog</title>
      <mediaobject>
        <imageobject>
          <imagedata fileref="images/menus/file/open.png" format="PNG"/>
        </imageobject>
      </mediaobject>
    </figure>

    <para>
      This file browser should look familiar when you have worked with files
      before. We will explain the details of this dialog below.
    </para>
    <orderedlist>
      <listitem><!--1-->
        <para>
          The <guibutton>Type a file name</guibutton> button toggles the
          visibility of the <guilabel>Location</guilabel> text box.
          By default this is hidden.
        </para>
        <para>
          The <keycombo><keycap>Ctrl</keycap><keycap>L</keycap></keycombo>
          key combination has the same action as this button.
        </para>
        <note>
          <para>
            When <guilabel>Search</guilabel> or <guilabel>Recently Used</guilabel>
            is selected in the <guilabel>Places</guilabel> list, it is not
            possible to show the <guilabel>Location</guilabel> text box.
            You will have to select another item from that list first.
          </para>
        </note>
      </listitem>

      <listitem><!--2-->
        <para>
          In the <guilabel>Location</guilabel> text box you can type a path to
          an image file. If you don't type any path, the name of the selected
          file will be displayed. You can also type the first letters of the
          name: it will be auto-completed and a list of file names beginning
          with these letters will be displayed.
        </para>
        <para>
          When you search for a file or directory using the
          <guilabel>Search</guilabel> feature (see below, item 4), the label
          changes to <guilabel>Search</guilabel> and you can enter the name in
          this text box.
        </para>
      </listitem>

      <!--
      <listitem><!-OLD 3->
        <para>
          <guibutton>Create a folder</guibutton> button: this button lets
          you add a new folder in the current folder. For some strange
          reason, this button is not always present and you can't remove
          this new folder.
        </para>
        XXX: Probably "Create folder" is only visible if you *save* a file
        - using the "Save Image" dialog.
      </listitem>
      -->

      <listitem><!--3-->
        <para>
          The path to the current folder is displayed here. You can use this
          to navigate to another folder by clicking on the desired part.
        </para>
      </listitem>

      <listitem><!--4-->
        <para>
          With <guilabel>Search</guilabel> you can look for a file or
          directory, even if you don't know the exact name.
          Click on <guilabel>Search</guilabel>, type a file name or even just a
          part of a name in the search box, and press <keycap>Enter</keycap>.
          The file display area (7) will then list all files and directories
          of your home directory (user directory on Windows) with names
          containing the text you searched for. Unfortunately you can't
          restrict the results to files of a specified type (10).
        </para>
        <para>
          <guilabel>Recently used</guilabel> shows the list of files you
          have recently opened.
        </para>
      </listitem>

      <listitem><!--5-->
        <para>
          This area gives you access to your main folders and your storage
          devices.
        </para>
      </listitem>

      <listitem><!--6-->
        <para>
          This part of the list shows the folders that you have bookmarked.
          You can add bookmarks by using the <guibutton>+</guibutton> (see 9),
          or the <guilabel>Add to Bookmarks</guilabel> option that is shown
          when you right-click a folder in the central panel.
        </para>
        <para>
          You can remove a bookmark by clicking the <guibutton>-</guibutton>,
          or the <guilabel>Remove</guilabel> option that is shown when you
          right click on a bookmark. This right click menu can also be used
          to <guilabel>Rename</guilabel> the bookmark.
        </para>
      </listitem>

      <listitem><!--7-->
        <para>
          The contents of the selected folder is displayed here. Change your
          current folder by double left clicking on a folder in this
          panel. Select a file with a single left click. You can then open
          the file you have selected by   clicking on the
          <guibutton>Open</guibutton> button.
          A double left click opens the file directly. Please note that you
          can open image files only.
        </para>
        <para>
          Right-clicking a folder name opens a context menu:
        </para>

        <screenshot>
          <mediaobject>
            <imageobject>
              <imagedata format="PNG"
                fileref="images/menus/file/open/folder-context-menu.png"/>
            </imageobject>
            <caption>The folder context menu</caption>
          </mediaobject>
        </screenshot>
      </listitem>

      <listitem><!--8-->
        <para>
          The selected image is displayed in the <guilabel>Preview</guilabel>
          window. If it is an image created by <acronym>GIMP</acronym>, file
          size, resolution and image composition are displayed below the
          preview window.
        </para>

        <tip>
          <para>
            If your image has been modified by another program, you can click
            on the Preview window to update the preview thumbnail.
          </para>
        </tip>
      </listitem>

      <listitem><!--9-->
        <para>
          By clicking the <guibutton>+</guibutton> button, you add the
          selected folder to bookmarks.
        </para>
        <para>
          By clicking the <guibutton>-</guibutton>, you remove the
          selected bookmark from the list.
        </para>
      </listitem>

      <listitem><!--10-->
        <para>
          This part does not exist anymore. To limit the shown images to
          only a certain file type, select one from the list below.
        </para>
      </listitem>

      <listitem><!--11-->
        <para>
          <guilabel>Select File Type</guilabel> can be used to explicitly set
          the image file format. In general you don't need to worry about this,
          because in most cases <acronym>GIMP</acronym> can determine the file
          type automatically.
        </para>
        <para>
          <!-- Added the below anchor to make sure we don't get a help page
               not found when pressing F1 in this part of the dialog. -->
          <anchor id="gimp-file-open-by-extension"/>
          The default is <guilabel>Automatically Detected</guilabel>, which
          causes <acronym>GIMP</acronym> to try to automatically detect
          the correct format.
        </para>
        <para>In the rare cases where neither the file extension nor internal
          information in the file are enough to tell <acronym>GIMP</acronym>
          the file type, you can set it by selecting it from this list.
        </para>
      </listitem>
    </orderedlist>
  </sect3>
</sect2>
