<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.3//EN"
                       "http://www.docbook.org/xml/4.3/docbookx.dtd">
<!-- section history:
  2017-10-12 j.h: created
-->
<sect1 id="gimp-filters-common">
  <title>Common Features</title>

  <para>
    Since <acronym>GIMP</acronym> 2.10, most filters are <acronym>GEGL</acronym>
    filters. These have several options in common:
  </para>
  <figure>
    <title>Common Options of GEGL Filters</title>
    <mediaobject>
      <imageobject>
        <imagedata format="PNG"
          fileref="images/filters/common/filter-options-common.png"/>
      </imageobject>
    </mediaobject>
  </figure>

  <variablelist>
    <varlistentry>
      <term>Presets</term>
      <listitem>
        <para>
          Filter presets are similar to tool presets, in that you can save
          your favorite settings and recall them when needed. They consist
          of:
        </para>
        <itemizedlist>
          <listitem>
            <para>
              A <emphasis role="bold">drop down list</emphasis> that shows the
              current preset and lets you choose a different one.
            </para>
          </listitem>
          <listitem>
            <para>
              <guiicon>
                <inlinegraphic 
                fileref="images/filters/common/preset-icon-add.png"/>
              </guiicon> 
              An icon to save the current settings as a named preset.
            </para>
          </listitem>
          <listitem>
            <para>
              <guiicon>
                <inlinegraphic
                 fileref="images/filters/common/preset-icon-manage.png"/>
              </guiicon> 
              An icon that opens a menu with options to manage presets.
            </para>
          </listitem>
        </itemizedlist>
      </listitem>
    </varlistentry>

    <varlistentry>
      <term>Input Type</term>
      <listitem>
        <note> 
          <para>
            The input type dropdown list is only visible when a selection
            is available.
          </para>
        </note>
        <itemizedlist>
          <listitem>
            <para>
              <guilabel>Use the selection as input</guilabel> If this option is
              selected, the filter only uses pixels inside the selection as
              input for the filter.
            </para>
            <mediaobject>
              <imageobject>
                <imagedata format="JPG"
                fileref="images/filters/use_selection_only.jpg"/>
              </imageobject>
            </mediaobject>
          </listitem>
          <listitem>
            <para>
              <guilabel>Use the entire layer as input</guilabel> If this option
              is selected, the input of the filter is the entire layer. The
              output will only affect the selection. The layer outside the
              selection remains unchanged.
            </para>
            <mediaobject>
              <imageobject>
                <imagedata format="JPG"
                fileref="images/filters/use_entire_layer.jpg"/>
              </imageobject>
            </mediaobject>
          </listitem>
        </itemizedlist>
      </listitem>
    </varlistentry>

    <varlistentry>
      <term>Blending Options</term>
      <listitem>
        <para>
          When you expand this option by clicking the +, you can choose the
          blend <guilabel>Mode</guilabel> to be used when applying the filter,
          and the <guilabel>Opacity</guilabel>.
          These work the same as the Layer
          <xref linkend="gimp-layer-dialog-paint-mode-menu"/> blending options.
        </para>
      </listitem>
    </varlistentry>

    <varlistentry>
      <term>Preview</term>
      <listitem>
        <para>
          When this option is enabled (default), changes in the filter settings
          are directly displayed on canvas. They are not applied to the image
          until you click the <guibutton>OK</guibutton> button.
        </para>
      </listitem>
    </varlistentry>
    
    <varlistentry>
      <term>Split view</term>
      <listitem>
        <para>
          When this option is enabled, the view of the image is divided in two
          parts. On the left side it shows the effect of the filter applied,
          and on the right side it shows the image without filter.
        </para>
        <note>
          <para>
            You can click-and-drag the line that divides the preview to move it,
            and <keycap>Ctrl</keycap>-click to make the line horizontal, or
            to switch it back to vertical.
          </para>
        </note>
      </listitem>
    </varlistentry>
    
  </variablelist>
</sect1>
